import React from 'react';
import './style.css';

const Button = () => {
    const btnRef = React.createRef();
    const onClickBtn = (e) => {
        btnRef.current.classList.add('animation');
    }
    return(
        <button type="button" className="button" onClick={onClickBtn} ref={btnRef}>
            Button
        </button>
    )
}

export default Button;