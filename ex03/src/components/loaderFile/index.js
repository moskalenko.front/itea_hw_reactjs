import React from 'react';
import './style.css';

const LoaderFile = () => {
    const imgRef = React.createRef();
    const handlerInputFile = (e) => {
        const input = e.target;

        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                imgRef.current.setAttribute('src',  e.target.result);
                imgRef.current.classList.add('show');
            }
        
            reader.readAsDataURL(input.files[0]);
        }
    }
    return(
        <div>
            <label>
                Нажмите, чтобы загрузить фото
                <input type="file" name="file" onChange={handlerInputFile} hidden/>
            </label>
            <div>
                <img  className="img" ref={imgRef} src="" alt="img" width="400"/> 
            </div>
        </div>
    )
}

export default LoaderFile;