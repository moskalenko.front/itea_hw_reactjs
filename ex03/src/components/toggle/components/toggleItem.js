import React from 'react';

const ToggleItem = ({name, parentName, active, onChangeStatus}) => {
    return(
      <div className={
        active === true ?
          "toggle__item active":
          "toggle__item"
        }
        data-value={name}
        data-parent={parentName}
        onClick={
            onChangeStatus !== undefined ?
                onChangeStatus :
                null
        }
        >
        {name}
      </div>
    );
  };

export default ToggleItem;