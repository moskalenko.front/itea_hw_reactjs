import React from 'react';
import PropTypes from 'prop-types';

const Input = ({type, placeholder, value, onChangeHandler, name}) => {
    return (
        <label>
            <div>{name}</div>
            <input
                type={type}
                name={name}
                placeholder={placeholder}
                value={value}
                onChange={onChangeHandler}
            />
        </label>
    )
}

export default Input;
Input.propTypes = {
    type: PropTypes.oneOf(['text', 'password', 'number']).isRequired,
    placeholder: PropTypes.string,
    value: PropTypes.any,
    onChangeHandler: PropTypes.func
};
