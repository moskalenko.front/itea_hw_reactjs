import React, { Component } from 'react';
import Button from './components/button';
import LoaderFile from './components/loaderFile';
import Toggle from './components/toggle'
import ToggleItem from './components/toggle/components/toggleItem'
import Input from './components/input'

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
       formData: {
          login: null,
          gender: 'male'
       },
    }
  }
  changeState = (event) => {
    let newToggleStatus = event.target.dataset.value;
    let toggleName = event.target.dataset.parent;
    this.setState({
      formData: {
          ...this.state.formData,
          [toggleName]: newToggleStatus
      }
    });
  }

  onChangeHandler = (event) => {
    let value = event.target.value;
    let name = event.target.name;

    this.setState({
      formData:{
      ...this.state.formData,
      [name]: value
      }
    });
  }
  render() {
    const {formData} = this.state;
    return (
      <div className="App">
        <Button/>
        <Button/>
        <Button/>
        <Button/>
        <form>
          <Input
            type="text"
            name="login"
            onChangeHandler = {this.onChangeHandler}
          />
          <Toggle 
            name = "gender"
            action = {this.changeState}
            activeState = {formData.gender}
          >
            <ToggleItem
                name="male"
            />
            <ToggleItem
                name="female"
            />
          </Toggle>
          <LoaderFile/>
        </form>
      </div>
    );
  }
}

export default App;
