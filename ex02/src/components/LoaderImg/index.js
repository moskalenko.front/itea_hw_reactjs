import React, {Component} from 'react';
import './style.css'

class LoaderImg extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isShowPreloader: true
        }
    }
    hidePreloader () {
        this.setState({
            isShowPreloader: false
        })
    }
    componentDidMount () {
        const nodeImg = document.getElementById('img');
        
        nodeImg.onload = () => {
            setTimeout(() => {
                this.hidePreloader();
            }, 500);
        }
    }
    render() {
        const {isShowPreloader} = this.state;
        const {imgUrl} = this.props;

        return (
            <div>
                <div 
                    className = { isShowPreloader ? 'preloader show' : 'preloader'}
                >
                    <div className='preloader__logo'>
                        ITEA
                    </div>
                </div>
                <img id='img' src={imgUrl} alt="img" />
            </div>
        );
    }
}

export default LoaderImg;