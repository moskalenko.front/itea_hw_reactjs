import React from 'react'
import './style.css'

function Cell({value, cells, background, color, type, currency}) {
    let nameClass = '';

    switch (type) {
        case "DATE":
            nameClass = 'date'
            break;
        case "NUMBER":
            nameClass = 'number'
            break;
        case "MONEY":
            nameClass = 'money';
            if (!currency) {
                console.error('Enter currency prop');
            }
            break;
        default:
    }
    console.log(nameClass);
    return(
        <td 
            style = {
                {
                    backgroundColor: background, 
                    color: color
                }
            }
            className = {nameClass}
            colSpan = {cells}
        >
            {value}
            {(type === 'MONEY') ? currency : null}
        </td>
    );
}

Cell.defaultProps = {
    value: 'lorem',
    type: 'TEXT',
    cells: 1,
    background: 'transparent',
    color: 'black'
}

export default Cell;