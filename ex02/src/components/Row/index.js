import React from 'react'

function Row({head, children}) {
    return(
        head ? 
            <thead>
                <tr>
                    {children}
                </tr>
            </thead>
        :
            <tr>
                {children}
            </tr>
    );
}

Row.defaultProps = {
    head: false
}

export default Row;