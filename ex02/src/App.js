import React, { Component } from 'react';
import LoaderImg from './components/LoaderImg'
import Cell from './components/Cell'
import Row from './components/Row'


const imgUrl = 'https://media.springernature.com/lw630/nature-cms/uploads/cms/pages/2913/top_item_image/cuttlefish-e8a66fd9700cda20a859da17e7ec5748.png';
class App extends Component {
  render() {
    return (
      <div className="App">
        <LoaderImg imgUrl={imgUrl}/>
        <table>
          <Row head={true}>
            <Cell cells="3"/>
            <Cell type="DATE"/>
            <Cell value="124.2" type="NUMBER"/>
          </Row>
          <tbody>
            <Row head={false}>
              <Cell value="300" type="MONEY" currency="$" />
              <Cell/>
              <Cell/>
            </Row>
            <Row>
              <Cell/>
              <Cell/>
              <Cell/>
            </Row>
          </tbody>
        </table>
      </div>
    );
  }
}

export default App;
